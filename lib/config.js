require('dotenv').config();

module.exports = {
	service: {
		name: process.env.SERVICE_NAME
	},
	api: {
		port: process.env.API_PORT,
		host: process.env.API_HOST,
		rateLimit: {
			window: process.env.API_RATELIMIT_WINDOW,
			requests: process.env.API_RATELIMIT_REQUESTS
		}
	},
	database: {
		user: process.env.DB_USER,
		password: process.env.DB_PASSWORD,
		host: process.env.DB_HOST,
		port: process.env.DB_PORT,
		database: process.env.DB_DATABASE
	}
};
