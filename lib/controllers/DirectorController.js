const {
    keysToCamelCase
} = require('../helpers/Tools');
const api = require('../util/api');

class DirectorController {

    static async getDirector(req, res, next) {
        try {
            // Get director from DB
            const [director] = (await db.execute('SELECT * FROM imdb.directors_get($1)', [
                req.params.directorId
            ]));

            // Respond from API
            return api.respond(res, api.json(`Director Detail Retrieved`, keysToCamelCase(director) ,'director'));
        } catch (e) {
            return next(e);
        }
    }

    static async getDirectors(req, res, next) {
        try {
            // Get directors from the DB
            const directors = (await db.execute('SELECT * FROM imdb.directors_get(NULL, $1, $2, $3)', [
                req.query.top,
                req.query.offset,
                req.query.name
            ]));

            // Respond from API
            return api.respond(res, api.json(`Directors List Retrieved`, directors.map(d => keysToCamelCase(d)), 'directors'));

        } catch (e) {
            return next(e);
        }
    }

    static async getDirectorMovies(req, res, next) {
        try {
            // Get director movie list from DB
            const movies = (await db.execute('SELECT * FROM imdb.director_movies($1, $2, $3, $4, $5, $6)', [
                req.params.directorId,
                req.query.top,
                req.query.offset,
                req.query.year,
                req.query.title,
                req.query.type
            ]));

            // Respond from API
            return api.respond(res, api.json(`Director Movie List Retrieved`, movies.map(m => keysToCamelCase(m)) ,'movies'));
        } catch (e) {
            return next(e);
        }
    }

}

module.exports = DirectorController;
