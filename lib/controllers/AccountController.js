const {
	ValidationError
} = require('jsonschema');

const {
	sha256
} = require('../helpers/Tools');
const api = require('../util/api');

class AccountController {

	static async registerUser(req, res, next) {
		try {
			// Check if password is OK
			if (req.body.password !== req.body.passwordConfirm) {
				return next(new ValidationError(`Password and Password-Confirm mismatch.`));
			}

			// Hash the password
			const passwordHash = sha256(req.body.password);

			// Register user to the DB
			const [user] = (await db.execute('SELECT * FROM imdb.user_register($1, $2, $3, $4)', [
				req.body.firstName,
				req.body.lastName,
				req.body.username,
				passwordHash
			]));

			// Respond from API
			return api.respond(res, api.json(`User Registered Successfully`, {
				id: user.id,
				firstName: user.first_name,
				lastName: user.last_name,
				username: user.username,
				apiKey: user.api_key
			}, 'user'));
		} catch (e) {
			return next(e);
		}
	}

	static async loginUser(req, res, next) {
		try {
			// Hash the password
			const passwordHash = sha256(req.body.password);

			// Get the user from the DB
			const [user] = (await db.execute('SELECT * FROM imdb.user_login($1, $2)', [
				req.body.username,
				passwordHash
			]));

			// Respond from API
			return api.respond(res, api.json(`User Logged In Successfully`, {
				id: user.id,
				firstName: user.first_name,
				lastName: user.last_name,
				username: user.username,
				apiKey: user.api_key
			}, 'user'));
		} catch (e) {
			return next(e);
		}
	}

	static async getUser(req, res, next) {
		try {

			// Get the user from the DB
			const [user] = (await db.execute('SELECT * FROM imdb.user_get($1)', [
				req.user.id
			]));

			// Respond from API
			return api.respond(res, api.json(`User Retrieved`, {
				id: user.id,
				firstName: user.first_name,
				lastName: user.last_name,
				username: user.username,
				apiKey: user.api_key
			}, 'user'));

		} catch (e) {
			return next(e);
		}
	}

}

module.exports = AccountController;
