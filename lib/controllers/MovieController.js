const {
	keysToCamelCase
} = require('../helpers/Tools');
const api = require('../util/api');

class MovieController {

	static async recommend(req, res, next)
	{
        try {
            // Get recommendations from DB
            const movies = (await db.execute('SELECT * FROM imdb.recommend($1, $2)', [
                req.user.id,
				req.query.limit
            ]));

            // Respond from API
            return api.respond(res, api.json(`Recommendations Retrieved`, movies.map(m => keysToCamelCase(m)), 'movies'));

        } catch (e) {
            return next(e);
        }
	}

	static async getMovie(req, res, next) {
		try {
			// Get the Movie from DB
			const [movie] = (await db.execute('SELECT * FROM imdb.movies_get($1)', [
				req.params.movieId
			]));

			// Respond from API
			return api.respond(res, api.json(`Movie Detail Retrieved`, keysToCamelCase(movie), 'movie'));

		} catch (e) {
			return next(e);
		}
	}

	static async getMovies(req, res, next) {
		try {
			// Get Movies from the DB
			const movies = (await db.execute('SELECT * FROM imdb.movies_get(NULL, $1, $2, $3, $4, $5)', [
				req.query.top,
				req.query.offset,
				req.query.year,
				req.query.title,
				req.query.type
			]));

			// Respond from API
			return api.respond(res, api.json(`Movies List Retrieved`, movies.map(m => keysToCamelCase(m)), 'movies'));

		} catch (e) {
			return next(e);
		}
	}

	static async getLikedMovies(req, res, next) {
		try {
			// Get Liked Movies from the DB
			const movies = (await db.execute('SELECT * FROM imdb.movies_get_liked($1)', [
				req.user.id
			]));

			// Respond from API
			return api.respond(res, api.json(`Liked Movies List Retrieved`, movies.map(m => keysToCamelCase(m)), 'likedMovies'));
		} catch (e) {
			return next(e);
		}
	}

	static async likeMovie(req, res, next) {
		try {
			// Call the like procedure
			await db.execute('SELECT * FROM imdb.movie_like($1, $2)', [
				req.user.id,
				req.params.movieId
			]);

			// Get Liked Movies from the DB
			const movies = (await db.execute('SELECT * FROM imdb.movies_get_liked($1)', [
				req.user.id
			]));

			// Respond from API
			return api.respond(res, api.json(`Movie liked`, movies.map(m => keysToCamelCase(m)), 'likedMovies'));
		} catch (e) {
			return next(e);
		}
	}

	static async unlikeMovie(req, res, next) {
		try {
			// Call the unlike procedure
			await db.execute('SELECT * FROM imdb.movie_unlike($1, $2)', [
				req.user.id,
				req.params.movieId
			]);

			// Get Liked Movies from the DB
			const movies = (await db.execute('SELECT * FROM imdb.movies_get_liked($1)', [
				req.user.id
			]));

			// Respond from API
			return api.respond(res, api.json(`Movie unliked`, movies.map(m => keysToCamelCase(m)), 'likedMovies'));
		} catch (e) {
			return next(e);
		}
	}

	static async getDirectors(req, res, next) {
		try {
			// Call the procedure
			const directors = await db.execute('SELECT * FROM imdb.movie_get_directors($1)', [
				req.params.movieId
			]);

			// Respond from API
			return api.respond(res, api.json(`Directors of the Movie Retrieved`, directors.map(d => keysToCamelCase(d)), 'directors'));
		} catch (e) {
			return next(e);
		}
	}

}

module.exports = MovieController;
