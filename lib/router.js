const express = require('express');
const api = require('./util/api');
const config = require('./config');
const validate = require('./middleware/validate');
const authenticate = require('./middleware/authenticate');
const mapParams = require('./middleware/mapParams');
const router = new express.Router();

// Controllers
const AccountController = require('./controllers/AccountController');
const MovieController = require('./controllers/MovieController');
const DirectorController = require('./controllers/DirectorController');

router.get('/ping', (req, res, next) => {
	try {
		return api.respond(res, api.json(`Pong! The service '${config.service.name}' is running.`));
	} catch (e) {
		return next(e);
	}
});

router.post('/register',
	validate('body', require('./models/user-register')),
	AccountController.registerUser
);

router.post('/login',
	validate('body', require('./models/user-login')),
	AccountController.loginUser
);

router.get('/me',
	authenticate(),
	AccountController.getUser
);

router.get('/movie/:movieId(\\d+)',
	authenticate(),
	mapParams('params', (p) => parseInt(p), 'movieId'),
	validate('params', require('./models/movie-detail')),
	MovieController.getMovie
	);

router.get('/movie/liked',
	authenticate(),
	MovieController.getLikedMovies
);

router.post('/movie/:movieId(\\d+)/like',
	authenticate(),
	mapParams('params', (p) => parseInt(p), 'movieId'),
	validate('params', require('./models/movie-detail')),
	MovieController.likeMovie
);

router.post('/movie/:movieId(\\d+)/unlike',
	authenticate(),
	mapParams('params', (p) => parseInt(p), 'movieId'),
	validate('params', require('./models/movie-detail')),
	MovieController.unlikeMovie
);

router.get('/movie/:movieId(\\d+)/director',
	authenticate(),
	mapParams('params', (p) => parseInt(p), 'movieId'),
	validate('params', require('./models/movie-detail')),
	MovieController.getDirectors
);

router.get('/movie',
	authenticate(),
	mapParams('query', (p) => parseInt(p), 'top', 'offset', 'year'),
	validate('query', require('./models/movie-search')),
	MovieController.getMovies
);

router.get('/director/:directorId(\\d+)/movie',
    authenticate(),
    mapParams('params', (p) => parseInt(p), 'directorId'),
    mapParams('query', (p) => parseInt(p), 'top', 'offset', 'year'),
    validate('params', require('./models/director-detail')),
    validate('query', require('./models/movie-search')),
    DirectorController.getDirectorMovies
);

router.get('/director/:directorId(\\d+)',
    authenticate(),
    mapParams('params', (p) => parseInt(p), 'directorId'),
    validate('params', require('./models/director-detail')),
    DirectorController.getDirector
);

router.get('/director',
    authenticate(),
    mapParams('query', (p) => parseInt(p), 'top', 'offset'),
    validate('query', require('./models/director-search')),
    DirectorController.getDirectors
);

router.get('/recommend',
    authenticate(),
	mapParams('query', (p) => parseInt(p), 'limit'),
	validate('query', require('./models/recommend')),
    MovieController.recommend
);

module.exports = router;
