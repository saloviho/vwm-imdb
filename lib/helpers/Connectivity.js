const config = require('../config');
const debug = require('debug')(config.service.name);
const { Pool } = require('pg');

async function connectPostgres() {
	global.dbPool = new Pool({
		user: config.database.user,
		host: config.database.host,
		database: config.database.database,
		password: config.database.password,
		port: config.database.port,
	});
	global.db = {};
	global.db.execute = (query, params) => {
		return new Promise((resolve, reject) => {
			global.dbPool.connect((err, client, release) => {
				if (err) {
					reject(err);
				} else {
					client.query(query, params, (err, result) => {
					release();
					if (err) {
						reject(err);
					} else {
						resolve(result.rows);
					}
				})
				}
			})
		});
	}
}

async function disconnectPostgres() {
	debug('Closing database connection');
	return new Promise(async(resolve) => {
		await global.dbPool.end();
		debug('Database connection closed');
		resolve();
	});
}

module.exports = { connectPostgres, disconnectPostgres };
