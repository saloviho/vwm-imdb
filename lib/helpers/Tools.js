const crypto = require('crypto');
const {camelCase} = require('lodash');

function sha256(raw) {
	return crypto.createHash('sha256').update(raw).digest('hex');
}

function keysToCamelCase(raw) {
	return Object.keys(raw).reduce((acc, curr) => {
		acc[camelCase(curr)] = raw[curr];
		return acc;
	}, {});
}

module.exports = {sha256, keysToCamelCase};
