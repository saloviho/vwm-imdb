const config = require('../config');
const debug = require('debug')(config.service.name);

function onError(error) {
	if (error.syscall !== 'listen') {
		throw error;
	}
	const bind = typeof config.api.port === 'string' ? 'Pipe ' + config.api.port : 'Port ' + config.api.port;
	switch (error.code) {
		case 'EACCES':
			console.error(bind + ' requires elevated privileges');
			process.exit(1);
			break;
		case 'EADDRINUSE':
			console.error(bind + ' is already in use');
			process.exit(1);
			break;
		default:
			throw error;
	}
}

function onListening(server) {
	let addr = server.address();
	let bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
	debug('Listening on ' + bind);
}

async function shutdownExpress(server) {
	debug('Shutting down Express gracefully');
	return new Promise(resolve => {
		server.close(() => {
			debug('Express shutdown successful');
			resolve();
		});
	});
}


module.exports = {onError, onListening, shutdownExpress};
