module.exports = {
	respond: (res, body, status = 200, type = 'application/json') => {
		res.set({
			'Content-Type': type,
			'Access-Control-Allow-Origin': '*'
		});
		res.status(status);
		if(type === 'application/json') {
			return res.json(body);
		} else {
			return res.send(body);
		}
	},
	json: (message, data = undefined, dataLabel = 'data') => {
		const out = {
			message
		};
		out[dataLabel] = data;
		return out;
	}
};
