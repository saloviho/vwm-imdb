const logger = require('morgan');

module.exports = logger((tokens, req, res) => {
	return [
		tokens.method(req, res),
		tokens.url(req, res),
		tokens.status(req, res),
		tokens.res(req, res, 'content-length'), '-',
		tokens['response-time'](req, res), 'ms'
	].join(' ');
}, {stream: {write: (msg) => console.debug(msg.slice(0, -1))}});
