const mapParams = (bundleLocation, fun, ...params) => {
	return async (req, res, next) => {
		try {
			params.forEach(param => {
				req[bundleLocation][param] && (req[bundleLocation][param] = fun(req[bundleLocation][param]));
			});
			return next();
		} catch (e) {
			return next(e);
		}
	};
};

module.exports = mapParams;
