const RateLimit = require('express-rate-limit');

module.exports = (time, requests) => {
	return new RateLimit({
		windowMs: time || 60000,
		max: requests
	});
};
