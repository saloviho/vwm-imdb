const validator = require('jsonschema').validate;

const validate = (bundleLocation, schema) => {
	return async (req, res, next) => {
		try {
			validator(req[bundleLocation], schema, {
				throwError: true
			});
			return next();
		} catch (e) {
			return next(e);
		}
	};
};

module.exports = validate;
