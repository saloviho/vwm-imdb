const authenticate = () => {
	return async (req, res, next) => {
		try {

			// Parse the header
			const header = req.headers.authorization;
			if (!header) {
				return next(new Error('Missing authorization header'));
			}

			// Extract key
			const key = header.slice(6);
			if (!key || key.length !== 36) {
				return next(new Error('Invalid API key format'));
			}

			// Get User from the DB by API key
			req.user = (await db.execute('SELECT * FROM imdb.user_authenticate($1)', [
				key
			]))[0];

			return next();
		} catch (e) {
			res.status(401);
			return next(e);
		}
	};
};

module.exports = authenticate;
