const express = require('express');
const config = require('./config');
const api = require('./util/api');
const http = require('http');
const debug = require('debug')(config.service.name);
require('express-async-errors');
const {
	ValidationError
} = require('jsonschema');
const {
	connectPostgres,
	disconnectPostgres
} = require('./helpers/Connectivity');
const {
	onError,
	onListening,
	shutdownExpress
} = require('./helpers/ServerTools');

const router = require('./router');

// Middleware
const logger = require('./middleware/logger');
const rateLimit = require('./middleware/rate-limit');
const cors = require('./middleware/cors');

// Express
const app = express()
	.use(logger)
	.use('/', rateLimit(config.api.rateLimit.window, config.api.rateLimit.requests))
	.use('/', cors)
	.use(express.json({limit: '100kb'}))
	.use(express.urlencoded({extended: false}))
	.set('port', config.api.port);

app.use(router);

// Handlers
app.use((req, res) => {
	api.respond(res, api.json('Not found.'), 404);
});

app.use((err, req, res, next) => {
	console.error(err.stack);
	if (err instanceof SyntaxError && err.status === 400 && 'body' in err) {
		return api.respond(res, api.json('Bad JSON body.', {
			description: `The request body couldn't be parsed as a valid JSON.`
		}), 400);
	}
	else if (err instanceof ValidationError) {
		return api.respond(res, api.json(err.message), 400);
	}
	else if (err.code === 'ER002') {
		return api.respond(res, api.json(err.message), 401);
	}
	else if (err.code === 'ER404') {
		return api.respond(res, api.json(err.message), 404);
	}
	else {
		return api.respond(res, api.json(err.message), 500);
	}
});

let server;
(async() => {
	await startup();
})();

process.on('SIGINT', shutdown);
process.on('SIGTERM', shutdown);
process.on('uncaughtException', shutdown);
process.on('unhandledRejection', shutdown);

async function startup() {
	server = http.createServer(app);
	server.on('error', onError);
	server.on('listening', () => onListening(server));

	debug('Initiating DB Pool');
	await connectPostgres();

	debug('Starting Express Server');
	server.listen(Number(config.api.port), config.api.host);
}

async function shutdown() {
	console.info('Initiating shutdown sequence...');
	await shutdownExpress(server);
	await disconnectPostgres();
	console.info('Shutdown sequence finished. Halting now.');
	process.exit(0);
}

module.exports = {app};
