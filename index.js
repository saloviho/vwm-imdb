let signalReceived = false;

const forceExit = (code = 0) => {
	console.info('Forcing process to exit');
	return process.exit(code);
};

const handleSignal = () => {
	if (signalReceived) return forceExit();
	signalReceived = true;
	console.info('Exiting process on signal...');
	setTimeout(forceExit, 5000).unref();
};

const handleUncaughtError = (err) => {
	console.error(err);
	console.info('Uncaught error handled, exiting process in 3 sec...');
	setTimeout(() => forceExit(1), 3000).unref();
};

process.on('SIGINT', handleSignal);
process.on('SIGTERM', handleSignal);
process.on('uncaughtException', handleUncaughtError);
process.on('unhandledRejection', handleUncaughtError);

(async () => {
	module.exports = require('./lib/app');
})();
