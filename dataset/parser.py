# -*- coding: utf-8 -*-

import requests
import json

class Parser:
    apiKey = "API_KEY"
    def parse1(self):
        f = open("result.txt", "ab")
        ids = open("tid.txt", "r")

        while True:
            line = ids.readline()
            if not line:
                break
            line = line.rstrip()
            apiUrl = f"http://www.omdbapi.com/?i={line}&apikey={self.apiKey}&type=movies&plot=full"
            responseBody = requests.get(apiUrl).text.encode("utf8")
            print(responseBody)

            movie = json.loads(responseBody)
            if "Title" in movie:
                f.write(responseBody)
                f.write("\n".encode("utf8"))
            elif "limit" in movie["Error"]:
                break

        f.close()

p = Parser()
p.parse()


