# -*- coding: utf-8 -*-
import json
import sys
import traceback

import psycopg2

class DB:
    conn = None
    cur = None
    def __init__(self):
        try:
            params = {
                'database': 'ENTER_DATABASE',
                'user': 'ENTER_USER',
                'password': 'ENTER_PASSWORD',
                'host': 'ENTER_HOST',
                'port': 'ENTER_PORT'
            }

            self.conn = psycopg2.connect(**params)
            self.cur = self.conn.cursor()
        except Exception as e:
            print(e)
            print("Connection Failed")

    def insertMovie(self, m):
        try:
            sql1 = 'insert into imdb.movie("title", "year", "rated", "released", "runtime", "actors", "plot", "language", "country", "poster", "type", "imdbId" ) values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s) RETURNING id'
            self.cur.execute(sql1, (m.title, m.year, m.rated, m.released, m.runtime, m.actors, m.plot, m.language, m.country, m.poster, m.type, m.imdbID))
            movie_id = self.cur.fetchone()[0]

            d_weight = 1/len(m.director)
            for d in m.director:
                sql2 = 'select id from imdb.director WHERE name=%s'
                self.cur.execute(sql2, (d,))
                result = self.cur.fetchone()
                if(result == None):
                    sql3 = 'insert into imdb.director("name") values (%s) RETURNING id'
                    self.cur.execute(sql3, (d,))
                    director_id = self.cur.fetchone()[0]
                else:
                    director_id = result[0]

                sql6 = 'insert into imdb.movie_director("movie_id", "director_id", "weight") values (%s, %s, %s)'
                self.cur.execute(sql6, (movie_id, director_id, d_weight))

            g_weight = 1/len(m.genre)
            for g in m.genre:
                sql4 = 'select id from imdb.genere WHERE name=%s'
                self.cur.execute(sql4, (g,))
                result = self.cur.fetchone()
                if (result == None):
                    sql5 = 'insert into imdb.genere("name") values (%s) RETURNING id'
                    self.cur.execute(sql5, (g,))
                    genere_id = self.cur.fetchone()[0]
                else:
                    genere_id = result[0]

                sql7 = 'insert into imdb.movie_genere("movie_id", "genere_id", "weight") values (%s, %s, %s)'
                self.cur.execute(sql7, (movie_id, genere_id, g_weight))
            self.conn.commit()
        except Exception as e:
            self.conn.rollback()
            traceback.print_stack()
            print(e)
            print(m.title)
            #sys.exit()


class Movie:
    title = ""
    year = ""
    rated = ""
    released = ""
    runtime = ""
    genre = []
    director = ""
    actors = []
    plot = ""
    language = ""
    country = ""
    poster = ""
    type = ""
    imdbID = ""

    def __init__(self, title, year, rated, released, runtime, genre, director, actors, plot, language, country, poster, type, imdbid):
        self.title = title
        self.year = year
        self.rated = rated
        self.released = released
        self.runtime = runtime
        self.genre = genre
        self.director = director
        self.actors = actors
        self.plot = plot
        self.language = language
        self.country = country
        self.poster = poster
        self.type = type
        self.imdbID = imdbid

db = DB()
f = open("result.txt", "rb")
for line in f:
    text = line.decode('utf-8')
    m = json.loads(text)
    for key in m:
        if(m[key] == "N/A"):
            m[key] = None

    if(m['Genre'] != None):
        genres = m['Genre'].split(", ")
        genres = set(genres)
        genres = list(genres)
    else:
        genres = None

    if(m['Actors'] != None):
        actors = m['Actors'].split(", ")
    else:
        actors = None

    if(m['Director'] != None):
        directors = m['Director'].split(", ")
        directors = set(directors)
        directors = list(directors)
    else:
        directors = None

    if(directors == None or genres == None):
        continue

    movie = Movie(m['Title'], m['Year'], m['Rated'], m['Released'], m['Runtime'], genres, directors, actors, m['Plot'], m['Language'], m['Country'], m['Poster'], m['Type'], m['imdbID'])
    db.insertMovie(movie)
