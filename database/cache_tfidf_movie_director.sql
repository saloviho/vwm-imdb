CREATE OR REPLACE FUNCTION imdb.cache_tfidf_movie_director()
    RETURNS void
    LANGUAGE plpgsql
    SECURITY DEFINER AS
$$
DECLARE
    _count_movies integer := (SELECT COUNT(1)
                              FROM imdb.movie m);
BEGIN

    CREATE TEMP TABLE temp_movie_director_counts ON COMMIT DROP AS
    SELECT d.id director_id, count(md.movie_id) total
    FROM imdb.director d
             INNER JOIN imdb.movie_director md ON d.id = md.director_id
    GROUP BY d.id;

    UPDATE imdb.movie_director md
    SET tfidf = md.weight * LN(
                _count_movies::double precision / (SELECT t.total
                                                   FROM temp_movie_director_counts t
                                                   WHERE t.director_id = md.director_id)
        );

END;
$$;
