CREATE OR REPLACE FUNCTION imdb.user_login(
    _username character varying,
    _password_hash character varying,
    OUT id integer,
    OUT first_name character varying,
    OUT last_name character varying,
    OUT username character varying,
    OUT api_key uuid
) RETURNS record
    LANGUAGE plpgsql
    SECURITY DEFINER AS
$$
BEGIN

    IF NOT EXISTS(SELECT 1 FROM imdb.user u WHERE u.username = _username AND u.password_hash = _password_hash) THEN
        RAISE EXCEPTION 'Invalid User.' USING ERRCODE = 'ER001';
    ELSE
        SELECT u.id, u.first_name, u.last_name, u.username, u.api_key
        INTO
            id, first_name, last_name, username, api_key
        FROM imdb.user u
        WHERE u.username = _username AND u.password_hash = _password_hash;
    END IF;

END;
$$;
