CREATE OR REPLACE FUNCTION imdb.user_authenticate(
    _api_key uuid,
    OUT id integer,
    OUT first_name character varying,
    OUT last_name character varying,
    OUT username character varying,
    OUT api_key uuid
) RETURNS record
    LANGUAGE plpgsql
    SECURITY DEFINER AS
$$
BEGIN

    IF NOT EXISTS(SELECT 1 FROM imdb.user u WHERE u.api_key = _api_key) THEN
        RAISE EXCEPTION 'Not Authenticated.' USING ERRCODE = 'ER002';
    ELSE
        SELECT u.id, u.first_name, u.last_name, u.username, u.api_key
        INTO
            id, first_name, last_name, username, api_key
        FROM imdb.user u
        WHERE u.api_key = _api_key;
    END IF;

END;
$$;
