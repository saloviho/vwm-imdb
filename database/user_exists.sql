CREATE OR REPLACE FUNCTION imdb.user_exists(_id integer)
    RETURNS void
    LANGUAGE plpgsql
    STABLE SECURITY DEFINER
AS $function$
BEGIN

    -- Metafunction. Has no output, but raises an exception when the user doesn't exist

    IF NOT EXISTS(
            SELECT 1
            FROM imdb.user u
            WHERE u.id = _id
        )
    THEN
        RAISE EXCEPTION 'User does not exist.' USING ERRCODE = 'ER404';
    END IF;


END;
$function$;
