CREATE OR REPLACE FUNCTION imdb.directors_get(_id integer DEFAULT NULL,
                                           _top smallint DEFAULT 10::smallint,
                                           _offset integer DEFAULT 0,
                                           _name character varying DEFAULT NULL,
                                           OUT id integer,
                                           OUT name character varying)
    RETURNS setof record
    STABLE
    LANGUAGE plpgsql
    SECURITY DEFINER AS
$$
BEGIN

    -- Verify if the director exists
    IF _id IS NOT NULL THEN
        PERFORM imdb.director_exists(_id);
    END IF;

    IF _top IS NULL THEN
        _top := 10::smallint;
    END IF;

    IF _offset IS NULL THEN
        _offset := 0;
    END IF;

    RETURN QUERY
        SELECT d.id,
               d.name
        FROM imdb.director d
        WHERE (_id IS NULL OR d.id = _id)
          AND (_name IS NULL OR to_tsvector(d.name) @@ plainto_tsquery(_name))
        LIMIT _top
        OFFSET
        _offset;

END;
$$;
