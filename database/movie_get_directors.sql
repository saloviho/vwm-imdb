CREATE OR REPLACE FUNCTION imdb.movie_get_directors(_movie_id integer,
                                                   OUT id integer,
                                                   OUT name character varying)
RETURNS SETOF record
    LANGUAGE plpgsql
STABLE
SECURITY DEFINER AS
    $$
    BEGIN

        RETURN QUERY
        SELECT d.id, d.name FROM imdb.director d WHERE d.id IN (SELECT q.director_id FROM imdb.movie_director q WHERE q.movie_id = _movie_id);

    END;
    $$;
