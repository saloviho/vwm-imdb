CREATE OR REPLACE FUNCTION imdb.movie_like(_user_id integer, _movie_id integer)
    RETURNS void
    LANGUAGE plpgsql
    SECURITY DEFINER AS
$$
DECLARE
    _count integer;
    _iterator record;
    _current_rate double precision := 0.0;
    _current_count integer := 0;
BEGIN

    -- Check if movie is liked already
    IF EXISTS (SELECT 1 FROM imdb.user_movie um WHERE um.movie_id = _movie_id AND um.user_id = _user_id) THEN
        -- If so, die
        RAISE EXCEPTION 'Movie ''%'' is liked by User ''%'' already!', _movie_id, _user_id;
    END IF;

    -- Else insert now
    INSERT INTO imdb.user_movie (user_id, movie_id) VALUES (_user_id, _movie_id);


    -- GENERES

    -- Count generes
    SELECT count(mg.movie_id) INTO _count FROM imdb.movie_genere mg WHERE mg.movie_id = _movie_id;

    -- For each genere of the movie
    FOR _iterator IN (SELECT mg.* FROM imdb.movie_genere mg WHERE mg.movie_id = _movie_id) LOOP

            -- If genere not in user_genere, create new
            IF NOT EXISTS (SELECT 1 FROM imdb.user_genere ug WHERE ug.user_id = _user_id AND ug.genere_id = _iterator.genere_id) THEN
                INSERT INTO imdb.user_genere (user_id, genere_id, rate, count) VALUES (_user_id, _iterator.genere_id, (1.0 / _count) , 1);
            ELSE
                -- Set current values
                SELECT ug.rate, ug.count INTO _current_rate, _current_count FROM imdb.user_genere ug WHERE ug.user_id = _user_id AND ug.genere_id = _iterator.genere_id;

                -- Update current values
                UPDATE imdb.user_genere ug
                SET rate = (_current_rate + (1.0 / _count)), count = (_current_count + 1)
                WHERE ug.user_id = _user_id AND ug.genere_id = _iterator.genere_id;
            END IF;

        END LOOP;

    -- DIRECTORS

    -- Count directors
    SELECT count(md.director_id) INTO _count FROM imdb.movie_director md WHERE md.movie_id = _movie_id;

    -- For each director of the movie
    FOR _iterator IN (SELECT md.* FROM imdb.movie_director md WHERE md.movie_id = _movie_id) LOOP

            -- If director not in user_director, create new
            IF NOT EXISTS (SELECT 1 FROM imdb.user_director ud WHERE ud.user_id = _user_id AND ud.director_id = _iterator.director_id) THEN
                INSERT INTO imdb.user_director (user_id, director_id, rate, count) VALUES (_user_id, _iterator.director_id, (1.0 / _count) , 1);
            ELSE
                -- Set current values
                SELECT ud.rate, ud.count INTO _current_rate, _current_count FROM imdb.user_director ud WHERE ud.user_id = _user_id AND ud.director_id = _iterator.director_id;

                -- Update current values
                UPDATE imdb.user_director ud
                SET rate = (_current_rate + (1.0 / _count)), count = (_current_count + 1)
                WHERE ud.user_id = _user_id AND ud.director_id = _iterator.director_id;
            END IF;

        END LOOP;
    PERFORM imdb.cache_tfidf_user_genere();
    PERFORM imdb.cache_tfidf_user_director();
END;
$$;
