CREATE OR REPLACE FUNCTION imdb.director_exists(_id integer)
    RETURNS void
    LANGUAGE plpgsql
    STABLE
    SECURITY DEFINER AS
$$
BEGIN

    -- Metafunction. Has no output, but raises an exception when the movie doesn't exist

    IF NOT EXISTS(
            SELECT 1
            FROM imdb.director d
            WHERE d.id = _id
        )
    THEN
        RAISE EXCEPTION 'Director does not exist.' USING ERRCODE = 'ER404';
    END IF;


END;
$$;
