CREATE OR REPLACE FUNCTION imdb.cache_tfidf_movie_genere()
    RETURNS void
    LANGUAGE plpgsql
    SECURITY DEFINER AS
$$
DECLARE
    _count_movies integer := (SELECT COUNT(1)
                              FROM imdb.movie m);
BEGIN

    CREATE TEMP TABLE temp_movie_genere_counts ON COMMIT DROP AS
    SELECT g.id genere_id, count(mg.movie_id) total
    FROM imdb.genere g
             INNER JOIN imdb.movie_genere mg ON g.id = mg.genere_id
    GROUP BY g.id;

    UPDATE imdb.movie_genere mg
    SET tfidf = mg.weight * LN(_count_movies::double precision / (SELECT t.total FROM temp_movie_genere_counts t WHERE t.genere_id = mg.genere_id));

END;
$$;
