CREATE OR REPLACE FUNCTION imdb.cache_tfidf_user_director()
    RETURNS void
    LANGUAGE plpgsql
    SECURITY DEFINER AS
$$
DECLARE
    _count_movies integer := (SELECT COUNT(1)
                              FROM imdb.movie m);
BEGIN
    CREATE TEMP TABLE IF NOT EXISTS temp_like_counts ON COMMIT DROP AS
    SELECT ug.user_id, SUM(ug.count + ud.count) as cnt FROM imdb.user_genere ug JOIN imdb.user_director ud ON ug.user_id = ud.user_id GROUP BY ug.user_id;

    CREATE TEMP TABLE temp_movie_director_counts ON COMMIT DROP AS
    SELECT d.id director_id, count(md.movie_id) total
    FROM imdb.director d
             INNER JOIN imdb.movie_director md ON d.id = md.director_id
    GROUP BY d.id;

    UPDATE imdb.user_director ud
    SET tfidf = ud.rate / (SELECT cnt FROM temp_like_counts lc WHERE ud.user_id = lc.user_id) * LN(_count_movies::double precision / (SELECT t.total FROM temp_movie_director_counts t WHERE t.director_id = ud.director_id));

END;
$$;