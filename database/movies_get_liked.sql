CREATE OR REPLACE FUNCTION imdb.movies_get_liked(_user_id integer,
                                                 OUT id integer,
                                                 OUT title character varying,
                                                 OUT year smallint,
                                                 OUT rated character varying,
                                                 OUT released date,
                                                 OUT runtime character varying,
                                                 OUT actors character varying[],
                                                 OUT plot character varying,
                                                 OUT language character varying,
                                                 OUT country character varying,
                                                 OUT poster character varying,
                                                 OUT type character varying,
                                                 OUT imdb_id character varying)
    RETURNS setof record
    STABLE
    LANGUAGE plpgsql
    SECURITY DEFINER AS
$$
BEGIN

    -- Verify if the user exists
    PERFORM imdb.user_exists(_user_id);

    RETURN QUERY
        SELECT m.id,
               m.title,
               m.year,
               m.rated,
               m.released,
               m.runtime,
               m.actors,
               m.plot,
               m.language,
               m.country,
               m.poster,
               m.type,
               m."imdbId"
        FROM imdb.movie m
        WHERE m.id IN (SELECT um.movie_id FROM imdb.user_movie um WHERE um.user_id = _user_id);

END;
$$;
