CREATE OR REPLACE FUNCTION imdb.director_movies(_id integer,
                                           _top smallint DEFAULT 10::smallint,
                                           _offset integer DEFAULT 0,
                                           _year smallint DEFAULT NULL,
                                           _title character varying DEFAULT NULL,
                                           _type character varying DEFAULT NULL,
                                           OUT id integer,
                                           OUT title character varying,
                                           OUT year smallint,
                                           OUT rated character varying,
                                           OUT released date,
                                           OUT runtime character varying,
                                           OUT actors character varying[],
                                           OUT plot character varying,
                                           OUT language character varying,
                                           OUT country character varying,
                                           OUT poster character varying,
                                           OUT type character varying,
                                           OUT imdb_id character varying)
    RETURNS setof record
    STABLE
    LANGUAGE plpgsql
    SECURITY DEFINER AS
$$
BEGIN

    -- Verify if director exists
    PERFORM imdb.director_exists(_id);

    IF _top IS NULL THEN
        _top := 10::smallint;
    END IF;

    IF _offset IS NULL THEN
        _offset := 0;
    END IF;

    RETURN QUERY
        SELECT m.id,
               m.title,
               m.year,
               m.rated,
               m.released,
               m.runtime,
               m.actors,
               m.plot,
               m.language,
               m.country,
               m.poster,
               m.type,
               m."imdbId"
        FROM imdb.movie m JOIN imdb.movie_director md ON m.id = md.movie_id
        WHERE (md.director_id = _id)
          AND (_year IS NULL OR m.year = _year)
          AND (_title IS NULL OR to_tsvector(m.title) @@ plainto_tsquery(_title))
          AND (_type IS NULL OR m.type = _type)
        LIMIT _top
        OFFSET
        _offset;

END;
$$;
