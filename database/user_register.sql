CREATE OR REPLACE FUNCTION imdb.user_register(
    _first_name character varying,
    _last_name character varying,
    _username character varying,
    _password_hash character varying,
    OUT id integer,
    OUT first_name character varying,
    OUT last_name character varying,
    OUT username character varying,
    OUT api_key uuid
) RETURNS record
    LANGUAGE plpgsql
    SECURITY DEFINER AS
$$
BEGIN

    INSERT INTO
        imdb.user (first_name, last_name, username, password_hash, api_key)
    VALUES (_first_name, _last_name, _username, _password_hash, uuid_generate_v4())
    RETURNING imdb.user.id, imdb.user.first_name, imdb.user.last_name, imdb.user.username, imdb.user.api_key
        INTO id, first_name, last_name, username, api_key;

END;
$$;
