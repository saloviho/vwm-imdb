CREATE OR REPLACE FUNCTION imdb.recommend(_id integer, _limit integer DEFAULT NULL::integer, OUT id integer, OUT title character varying, OUT year smallint, OUT rated character varying, OUT released date, OUT runtime character varying,
                                          OUT actors character varying[], OUT plot character varying, OUT language character varying, OUT country character varying, OUT poster character varying, OUT type character varying,
                                          OUT imdb_id character varying)
    RETURNS setof record
    LANGUAGE plpgsql
    SECURITY DEFINER
AS
$$

/*
For its purpose function use cosine similarity
Ai - genere/director vector from DB
Bi - user vector
cos a = Sum(Ai * Bi) / sqrt(Sum(Ai * Ai)) / sqrt(Sum(Bi * Bi));
*/

DECLARE
    /* Calculate Sum(Bi * Bi) */
    _user_sum double precision := (SELECT SUM(res)
                                   FROM ((SELECT SUM(ug.tfidf * ug.tfidf) AS res FROM imdb.user_genere ug WHERE ug.user_id = _id)
                                         UNION ALL
                                         (SELECT SUM(ud.tfidf * ud.tfidf) AS res FROM imdb.user_director ud WHERE ud.user_id = _id)) q);
BEGIN
    IF _limit IS NULL THEN
        _limit := 10;
    END IF;

    CREATE TEMP TABLE _user_generes ON COMMIT DROP AS
    SELECT * FROM imdb.user_genere ug WHERE ug.user_id = _id;

    CREATE TEMP TABLE _user_directors ON COMMIT DROP AS
    SELECT * FROM imdb.user_director ud WHERE ud.user_id = _id;

    /* Calculate Sum(Ai * Bi) for genere and director and then add them together */
    CREATE TEMP TABLE numerator ON COMMIT DROP AS
    SELECT gn.movie_id, (COALESCE(gn.res, 0) + COALESCE(dn.res, 0)) AS res
    FROM (SELECT mg.movie_id, SUM(mg.tfidf * ug.tfidf) AS res
          FROM imdb.movie_genere mg
                   JOIN _user_generes ug ON mg.genere_id = ug.genere_id
          GROUP BY mg.movie_id) gn
             FULL OUTER JOIN
         (SELECT md.movie_id, SUM(md.tfidf * ud.tfidf) AS res
          FROM imdb.movie_director md
                   JOIN _user_directors ud ON md.director_id = ud.director_id
          GROUP BY md.movie_id) dn
         ON gn.movie_id = dn.movie_id;

    /* Calculate Sum(Ai * Ai) for genere and director and then add them together */
    CREATE TEMP TABLE denomenator ON COMMIT DROP AS
    SELECT gd.movie_id, (COALESCE(gd.res, 0) + COALESCE(dd.res, 0)) AS res
    FROM (SELECT mg.movie_id, SUM(mg.tfidf * mg.tfidf) AS res
          FROM imdb.movie_genere mg
                   JOIN _user_generes ug ON mg.genere_id = ug.genere_id
          GROUP BY mg.movie_id) gd
             FULL OUTER JOIN
         (SELECT md.movie_id, SUM(md.tfidf * md.tfidf) AS res
          FROM imdb.movie_director md
                   JOIN _user_directors ud ON md.director_id = ud.director_id
          GROUP BY md.movie_id) dd
         ON gd.movie_id = dd.movie_id;

    /* Final calculation cos a = nomerator/sqtr(denomenator)/sqrt(_user_sum) */
    CREATE TEMP TABLE result ON COMMIT DROP AS
    SELECT n.movie_id, (n.res / sqrt(d.res) / sqrt(_user_sum)) AS score
    FROM (SELECT * FROM numerator) n
             JOIN (SELECT * FROM denomenator) d ON n.movie_id = d.movie_id
    WHERE n.movie_id NOT IN (SELECT um.movie_id FROM imdb.user_movie um WHERE um.user_id = _id)
    ORDER BY score DESC, n.movie_id DESC
    LIMIT _limit;

    RETURN QUERY SELECT m.id,
                        m.title,
                        m.year,
                        m.rated,
                        m.released,
                        m.runtime,
                        m.actors,
                        m.plot,
                        m.language,
                        m.country,
                        m.poster,
                        m.type,
                        m."imdbid"
                 FROM (SELECT movie_id FROM result) r
                          JOIN imdb.movie m ON r.movie_id = m.id;
END;
$$;
