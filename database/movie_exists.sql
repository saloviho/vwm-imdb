CREATE OR REPLACE FUNCTION imdb.movie_exists(_id integer)
    RETURNS void
    LANGUAGE plpgsql
    STABLE
    SECURITY DEFINER AS
$$
BEGIN

    -- Metafunction. Has no output, but raises an exception when the movie doesn't exist

    IF NOT EXISTS(
            SELECT 1
            FROM imdb.movie m
            WHERE m.id = _id
        )
    THEN
        RAISE EXCEPTION 'Movie does not exist.' USING ERRCODE = 'ER404';
    END IF;


END;
$$;
