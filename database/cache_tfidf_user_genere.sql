CREATE OR REPLACE FUNCTION imdb.cache_tfidf_user_genere()
    RETURNS void
    LANGUAGE plpgsql
    SECURITY DEFINER AS
$$
DECLARE
    _count_movies integer := (SELECT COUNT(1)
                              FROM imdb.movie m);
BEGIN
    CREATE TEMP TABLE IF NOT EXISTS temp_like_counts ON COMMIT DROP AS
    SELECT ug.user_id, SUM(ug.count + ud.count) as cnt FROM imdb.user_genere ug JOIN imdb.user_director ud ON ug.user_id = ud.user_id GROUP BY ug.user_id;

    CREATE TEMP TABLE temp_movie_genere_counts ON COMMIT DROP AS
    SELECT g.id genere_id, count(mg.movie_id) total
    FROM imdb.genere g
             INNER JOIN imdb.movie_genere mg ON g.id = mg.genere_id
    GROUP BY g.id;

    UPDATE imdb.user_genere ug
    SET tfidf = ug.rate / (SELECT cnt FROM temp_like_counts lc WHERE ug.user_id = lc.user_id) * LN(_count_movies::double precision / (SELECT t.total FROM temp_movie_genere_counts t WHERE t.genere_id = ug.genere_id));

END;
$$;