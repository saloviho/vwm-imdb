# Lokální prostředí - Docker

- První spuštění:
		- `mkdir postgres-data` v tomhle adresáři
		- `docker-compose up -d`
		- do databáze se přihlásit jako uživatel `postgres` s heslem `postgres` a pustit v ní skript `vwm-imdb.sql`
		- skript vytvoří schéma `imdb` a v něm všechny potřebné tabulky a tak dále
		- pak se z databáze můžeme odhlásit a `docker-compose down` zase vypnout kontejner

- Regulérní spuštění
		- `docker-compose up -d`
		- do databáze se po jejím vytvoření přihlašujeme jako uživatel `vwm-user` s heslem `fitcvut`, pod tímhle účtem poběží i aplikace
		- účet má práva jenom ve schématu `imdb`, pokud je potřeba udělat nějaké nastavení DB, je potřeba jít zase jako `postgres`:`postgres`, ale k tomu by neměl být důvod
		- pro ukončení `docker-compose down`

