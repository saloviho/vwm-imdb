-- Database generated with pgModeler (PostgreSQL Database Modeler).
-- pgModeler  version: 0.9.2
-- PostgreSQL version: 12.0
-- Project Site: pgmodeler.io
-- Model Author: ---

-- object: "vwm-user" | type: ROLE --
-- DROP ROLE IF EXISTS "vwm-user";
CREATE ROLE "vwm-user" WITH 
	LOGIN
	ENCRYPTED PASSWORD 'fitcvut';
-- ddl-end --
COMMENT ON ROLE "vwm-user" IS E'User for the VWM application';
-- ddl-end --


-- Database creation must be done outside a multicommand file.
-- These commands were put in this file only as a convenience.
-- -- object: "vwm-imdb" | type: DATABASE --
-- -- DROP DATABASE IF EXISTS "vwm-imdb";
-- CREATE DATABASE "vwm-imdb"
-- 	OWNER = postgres;
-- -- ddl-end --
-- 

-- object: imdb | type: SCHEMA --
-- DROP SCHEMA IF EXISTS imdb CASCADE;
CREATE SCHEMA imdb;
-- ddl-end --
ALTER SCHEMA imdb OWNER TO "vwm-user";
-- ddl-end --
COMMENT ON SCHEMA imdb IS E'VMW IMDB schema';
-- ddl-end --

SET search_path TO pg_catalog,public,imdb;
-- ddl-end --

-- object: imdb.movie_id_sequence | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS imdb.movie_id_sequence CASCADE;
CREATE SEQUENCE imdb.movie_id_sequence
	INCREMENT BY 1
	MINVALUE 0
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE imdb.movie_id_sequence OWNER TO "vwm-user";
-- ddl-end --
COMMENT ON SEQUENCE imdb.movie_id_sequence IS E'Sequence for generating Movie IDs';
-- ddl-end --

-- object: imdb.movie | type: TABLE --
-- DROP TABLE IF EXISTS imdb.movie CASCADE;
CREATE TABLE imdb.movie (
	id integer NOT NULL DEFAULT nextval('imdb.movie_id_sequence'::regclass),
	title character varying NOT NULL,
	year smallint,
	rated character varying,
	released date,
	runtime character varying,
	actors character varying[],
	plot character varying,
	language character varying,
	country character varying,
	poster character varying,
	type character varying,
	"imdbId" character varying,
	CONSTRAINT movie_pkey PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON TABLE imdb.movie IS E'Movies';
-- ddl-end --
COMMENT ON COLUMN imdb.movie.id IS E'Id of the movie';
-- ddl-end --
COMMENT ON COLUMN imdb.movie.title IS E'Name of the movie';
-- ddl-end --
COMMENT ON COLUMN imdb.movie.year IS E'Year of creation';
-- ddl-end --
COMMENT ON COLUMN imdb.movie.rated IS E'Rating of the movie';
-- ddl-end --
ALTER TABLE imdb.movie OWNER TO "vwm-user";
-- ddl-end --

-- object: imdb.director_id_sequence | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS imdb.director_id_sequence CASCADE;
CREATE SEQUENCE imdb.director_id_sequence
	INCREMENT BY 1
	MINVALUE 0
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE imdb.director_id_sequence OWNER TO "vwm-user";
-- ddl-end --
COMMENT ON SEQUENCE imdb.director_id_sequence IS E'Sequence for generating director IDs';
-- ddl-end --

-- object: imdb.director | type: TABLE --
-- DROP TABLE IF EXISTS imdb.director CASCADE;
CREATE TABLE imdb.director (
	id integer NOT NULL DEFAULT nextval('imdb.director_id_sequence'::regclass),
	name character varying NOT NULL,
	CONSTRAINT director_pkey PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON TABLE imdb.director IS E'Table of directors';
-- ddl-end --
COMMENT ON COLUMN imdb.director.id IS E'Id of the director';
-- ddl-end --
COMMENT ON COLUMN imdb.director.name IS E'Name if the director';
-- ddl-end --
ALTER TABLE imdb.director OWNER TO "vwm-user";
-- ddl-end --

-- object: imdb.genere_id_sequence | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS imdb.genere_id_sequence CASCADE;
CREATE SEQUENCE imdb.genere_id_sequence
	INCREMENT BY 1
	MINVALUE 0
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE imdb.genere_id_sequence OWNER TO "vwm-user";
-- ddl-end --
COMMENT ON SEQUENCE imdb.genere_id_sequence IS E'Sequence for generating Genere IDs';
-- ddl-end --

-- object: imdb.genere | type: TABLE --
-- DROP TABLE IF EXISTS imdb.genere CASCADE;
CREATE TABLE imdb.genere (
	id integer NOT NULL DEFAULT nextval('imdb.genere_id_sequence'::regclass),
	name character varying NOT NULL,
	CONSTRAINT genere_pkey PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON TABLE imdb.genere IS E'Table of generes';
-- ddl-end --
COMMENT ON COLUMN imdb.genere.id IS E'Id of the genere';
-- ddl-end --
COMMENT ON COLUMN imdb.genere.name IS E'Name of the genere';
-- ddl-end --
COMMENT ON CONSTRAINT genere_pkey ON imdb.genere  IS E'Primary key of genere';
-- ddl-end --
ALTER TABLE imdb.genere OWNER TO "vwm-user";
-- ddl-end --

-- object: imdb.movie_genere | type: TABLE --
-- DROP TABLE IF EXISTS imdb.movie_genere CASCADE;
CREATE TABLE imdb.movie_genere (
	movie_id integer NOT NULL,
	genere_id integer NOT NULL,
	weight double precision NOT NULL,
	tfidf double precision,
	CONSTRAINT movie_genere_pkey PRIMARY KEY (movie_id,genere_id)

);
-- ddl-end --
COMMENT ON TABLE imdb.movie_genere IS E'Connects movie and genere';
-- ddl-end --
COMMENT ON COLUMN imdb.movie_genere.movie_id IS E'Id of the movie';
-- ddl-end --
COMMENT ON COLUMN imdb.movie_genere.genere_id IS E'Id of the genere';
-- ddl-end --
COMMENT ON COLUMN imdb.movie_genere.weight IS E'How much should be this genere considered';
-- ddl-end --
COMMENT ON COLUMN imdb.movie_genere.tfidf IS E'TFIDF';
-- ddl-end --
ALTER TABLE imdb.movie_genere OWNER TO "vwm-user";
-- ddl-end --

-- object: imdb.movie_director | type: TABLE --
-- DROP TABLE IF EXISTS imdb.movie_director CASCADE;
CREATE TABLE imdb.movie_director (
	movie_id integer NOT NULL,
	director_id integer NOT NULL,
	tfidf double precision,
	CONSTRAINT movie_director_pkey PRIMARY KEY (movie_id,director_id)

);
-- ddl-end --
COMMENT ON TABLE imdb.movie_director IS E'Relation table for connecting movies with their directors';
-- ddl-end --
COMMENT ON COLUMN imdb.movie_director.movie_id IS E'Movie ID';
-- ddl-end --
COMMENT ON COLUMN imdb.movie_director.director_id IS E'Id of the Director';
-- ddl-end --
COMMENT ON COLUMN imdb.movie_director.tfidf IS E'TFIDF';
-- ddl-end --
ALTER TABLE imdb.movie_director OWNER TO "vwm-user";
-- ddl-end --

-- object: "movie_imbdId_index" | type: INDEX --
-- DROP INDEX IF EXISTS imdb."movie_imbdId_index" CASCADE;
CREATE UNIQUE INDEX "movie_imbdId_index" ON imdb.movie
	USING btree
	(
	  "imdbId"
	);
-- ddl-end --

-- object: genere_name_index | type: INDEX --
-- DROP INDEX IF EXISTS imdb.genere_name_index CASCADE;
CREATE UNIQUE INDEX genere_name_index ON imdb.genere
	USING btree
	(
	  name
	);
-- ddl-end --

-- object: imdb.user_id_sequence | type: SEQUENCE --
-- DROP SEQUENCE IF EXISTS imdb.user_id_sequence CASCADE;
CREATE SEQUENCE imdb.user_id_sequence
	INCREMENT BY 1
	MINVALUE 0
	MAXVALUE 2147483647
	START WITH 1
	CACHE 1
	NO CYCLE
	OWNED BY NONE;
-- ddl-end --
ALTER SEQUENCE imdb.user_id_sequence OWNER TO "vwm-user";
-- ddl-end --
COMMENT ON SEQUENCE imdb.user_id_sequence IS E'Sequence for generating user IDs';
-- ddl-end --

-- object: imdb."user" | type: TABLE --
-- DROP TABLE IF EXISTS imdb."user" CASCADE;
CREATE TABLE imdb."user" (
	id integer NOT NULL DEFAULT nextval('imdb.user_id_sequence'::regclass),
	first_name character varying NOT NULL,
	last_name character varying NOT NULL,
	username character varying NOT NULL,
	password_hash character varying NOT NULL,
	is_admin boolean NOT NULL DEFAULT false,
	api_key uuid NOT NULL,
	CONSTRAINT user_pkey PRIMARY KEY (id)

);
-- ddl-end --
COMMENT ON TABLE imdb."user" IS E'Table of users';
-- ddl-end --
COMMENT ON COLUMN imdb."user".id IS E'Id of the user';
-- ddl-end --
COMMENT ON COLUMN imdb."user".first_name IS E'First Name';
-- ddl-end --
COMMENT ON COLUMN imdb."user".last_name IS E'Last Name';
-- ddl-end --
COMMENT ON COLUMN imdb."user".password_hash IS E'Hash of the password';
-- ddl-end --
COMMENT ON COLUMN imdb."user".is_admin IS E'Is Admin?';
-- ddl-end --
COMMENT ON COLUMN imdb."user".api_key IS E'API key';
-- ddl-end --
ALTER TABLE imdb."user" OWNER TO "vwm-user";
-- ddl-end --

-- object: imdb.user_director | type: TABLE --
-- DROP TABLE IF EXISTS imdb.user_director CASCADE;
CREATE TABLE imdb.user_director (
	user_id integer NOT NULL,
	director_id integer NOT NULL,
	rate double precision NOT NULL DEFAULT 0,
	count integer NOT NULL DEFAULT 0,
	tfidf double precision,
	CONSTRAINT user_director_pkey PRIMARY KEY (user_id,director_id)

);
-- ddl-end --
COMMENT ON TABLE imdb.user_director IS E'User Director Relation';
-- ddl-end --
COMMENT ON COLUMN imdb.user_director.user_id IS E'User ID';
-- ddl-end --
COMMENT ON COLUMN imdb.user_director.director_id IS E'Director ID';
-- ddl-end --
COMMENT ON COLUMN imdb.user_director.count IS E'Number of times the director was liked through a movie';
-- ddl-end --
COMMENT ON COLUMN imdb.user_director.tfidf IS E'TFIDF';
-- ddl-end --
ALTER TABLE imdb.user_director OWNER TO "vwm-user";
-- ddl-end --

-- object: imdb.user_genere | type: TABLE --
-- DROP TABLE IF EXISTS imdb.user_genere CASCADE;
CREATE TABLE imdb.user_genere (
	user_id integer NOT NULL,
	genere_id integer NOT NULL,
	rate double precision NOT NULL DEFAULT 0,
	count integer NOT NULL DEFAULT 0,
	tfidf double precision,
	CONSTRAINT user_genere_pkey PRIMARY KEY (user_id,genere_id)

);
-- ddl-end --
COMMENT ON TABLE imdb.user_genere IS E'User Genere relation table';
-- ddl-end --
COMMENT ON COLUMN imdb.user_genere.count IS E'Number of times the genere was liked through a movie';
-- ddl-end --
COMMENT ON COLUMN imdb.user_genere.tfidf IS E'TFIDF';
-- ddl-end --
ALTER TABLE imdb.user_genere OWNER TO "vwm-user";
-- ddl-end --

-- object: user_username_unique | type: INDEX --
-- DROP INDEX IF EXISTS imdb.user_username_unique CASCADE;
CREATE UNIQUE INDEX user_username_unique ON imdb."user"
	USING btree
	(
	  username
	);
-- ddl-end --

-- object: imdb.user_movie | type: TABLE --
-- DROP TABLE IF EXISTS imdb.user_movie CASCADE;
CREATE TABLE imdb.user_movie (
	user_id integer NOT NULL,
	movie_id integer NOT NULL,
	CONSTRAINT user_movie_pkey PRIMARY KEY (user_id,movie_id)

);
-- ddl-end --
COMMENT ON TABLE imdb.user_movie IS E'Stores information about "User likes the movie"';
-- ddl-end --
COMMENT ON COLUMN imdb.user_movie.user_id IS E'Id of the user';
-- ddl-end --
COMMENT ON COLUMN imdb.user_movie.movie_id IS E'Id of the movie';
-- ddl-end --
COMMENT ON CONSTRAINT user_movie_pkey ON imdb.user_movie  IS E'Index on Like';
-- ddl-end --
ALTER TABLE imdb.user_movie OWNER TO "vwm-user";
-- ddl-end --

-- object: movie_genere_movie_id_fkey | type: CONSTRAINT --
-- ALTER TABLE imdb.movie_genere DROP CONSTRAINT IF EXISTS movie_genere_movie_id_fkey CASCADE;
ALTER TABLE imdb.movie_genere ADD CONSTRAINT movie_genere_movie_id_fkey FOREIGN KEY (movie_id)
REFERENCES imdb.movie (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --
COMMENT ON CONSTRAINT movie_genere_movie_id_fkey ON imdb.movie_genere  IS E'Foreign key to movie_id';
-- ddl-end --


-- object: movie_genere_genere_id | type: CONSTRAINT --
-- ALTER TABLE imdb.movie_genere DROP CONSTRAINT IF EXISTS movie_genere_genere_id CASCADE;
ALTER TABLE imdb.movie_genere ADD CONSTRAINT movie_genere_genere_id FOREIGN KEY (genere_id)
REFERENCES imdb.genere (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --
COMMENT ON CONSTRAINT movie_genere_genere_id ON imdb.movie_genere  IS E'Foreign key to genere_id';
-- ddl-end --


-- object: movie_director_movie_id_fkey | type: CONSTRAINT --
-- ALTER TABLE imdb.movie_director DROP CONSTRAINT IF EXISTS movie_director_movie_id_fkey CASCADE;
ALTER TABLE imdb.movie_director ADD CONSTRAINT movie_director_movie_id_fkey FOREIGN KEY (movie_id)
REFERENCES imdb.movie (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --
COMMENT ON CONSTRAINT movie_director_movie_id_fkey ON imdb.movie_director  IS E'Foreign key to movie_id';
-- ddl-end --


-- object: movie_director_director_id | type: CONSTRAINT --
-- ALTER TABLE imdb.movie_director DROP CONSTRAINT IF EXISTS movie_director_director_id CASCADE;
ALTER TABLE imdb.movie_director ADD CONSTRAINT movie_director_director_id FOREIGN KEY (director_id)
REFERENCES imdb.director (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --
COMMENT ON CONSTRAINT movie_director_director_id ON imdb.movie_director  IS E'Foreign key to director_id';
-- ddl-end --


-- object: user_director_user_id_fkey | type: CONSTRAINT --
-- ALTER TABLE imdb.user_director DROP CONSTRAINT IF EXISTS user_director_user_id_fkey CASCADE;
ALTER TABLE imdb.user_director ADD CONSTRAINT user_director_user_id_fkey FOREIGN KEY (user_id)
REFERENCES imdb."user" (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: user_director_director_id_fkey | type: CONSTRAINT --
-- ALTER TABLE imdb.user_director DROP CONSTRAINT IF EXISTS user_director_director_id_fkey CASCADE;
ALTER TABLE imdb.user_director ADD CONSTRAINT user_director_director_id_fkey FOREIGN KEY (director_id)
REFERENCES imdb.director (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: user_genere_user_id_fkey | type: CONSTRAINT --
-- ALTER TABLE imdb.user_genere DROP CONSTRAINT IF EXISTS user_genere_user_id_fkey CASCADE;
ALTER TABLE imdb.user_genere ADD CONSTRAINT user_genere_user_id_fkey FOREIGN KEY (user_id)
REFERENCES imdb."user" (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: user_genere_genere_id_fkey | type: CONSTRAINT --
-- ALTER TABLE imdb.user_genere DROP CONSTRAINT IF EXISTS user_genere_genere_id_fkey CASCADE;
ALTER TABLE imdb.user_genere ADD CONSTRAINT user_genere_genere_id_fkey FOREIGN KEY (genere_id)
REFERENCES imdb.genere (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --

-- object: user_movie_user_id_fkey | type: CONSTRAINT --
-- ALTER TABLE imdb.user_movie DROP CONSTRAINT IF EXISTS user_movie_user_id_fkey CASCADE;
ALTER TABLE imdb.user_movie ADD CONSTRAINT user_movie_user_id_fkey FOREIGN KEY (user_id)
REFERENCES imdb."user" (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --
COMMENT ON CONSTRAINT user_movie_user_id_fkey ON imdb.user_movie  IS E'Reference to user';
-- ddl-end --


-- object: user_movie_movie_id_fkey | type: CONSTRAINT --
-- ALTER TABLE imdb.user_movie DROP CONSTRAINT IF EXISTS user_movie_movie_id_fkey CASCADE;
ALTER TABLE imdb.user_movie ADD CONSTRAINT user_movie_movie_id_fkey FOREIGN KEY (movie_id)
REFERENCES imdb.movie (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;
-- ddl-end --
COMMENT ON CONSTRAINT user_movie_movie_id_fkey ON imdb.user_movie  IS E'Reference to Movie';
-- ddl-end --



