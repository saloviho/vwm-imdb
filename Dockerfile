FROM node:12
RUN mkdir -p /var/app
ADD . /var/app
WORKDIR "/var/app"
RUN npm install
EXPOSE 3001
CMD ["node", "index"]
